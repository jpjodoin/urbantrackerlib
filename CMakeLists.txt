#Minimum version of CMAKE used
cmake_minimum_required(VERSION 2.8)
set(LIBOUTPUT bin/${CMAKE_BUILD_TYPE})

file(
    GLOB_RECURSE
	source_files
	src/*
	include/*
)

include_directories(include)

add_library(TrackingLib STATIC ${source_files})
target_link_libraries(TrackingLib ${OpenCV_LIBS} Boost::program_options Boost::filesystem Boost::system BGSLib ${CMAKE_DL_LIBS} external) 
